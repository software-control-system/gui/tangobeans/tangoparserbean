/*******************************************************************************
 * Copyright (c) 2006 Synchrotron SOLEIL
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.All rights reserved. This program and the accompanying materials
 * 
 * Contributors: vincent.hardion@synchrotron-soleil.fr - initial implementation
 *******************************************************************************/
/*
 * Created on 10 ao�t 2005 with Eclipse
 */
package fr.soleil.bean.tangoparser;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.prefs.Preferences;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.swing.ComboBox;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.TextArea;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.comete.tango.data.service.helper.TangoAttributeHelper;
import fr.soleil.comete.tango.data.service.helper.TangoCommandHelper;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.IDataSourceProducer;
import fr.soleil.data.service.IKey;
import fr.soleil.lib.project.ObjectUtils;

/**
 * @author HARDION
 * @author AMESYS YLE : clean and migration to comete
 */
public class TangoParserBean extends AbstractTangoBox implements ActionListener {

    private static final long serialVersionUID = 4415449731169430525L;

    private static final Dimension VALUE_SIZE = new Dimension(30, 20);
    private static final Insets LEFT_GAP = new Insets(2, 5, 2, 2);
    private static final Insets DEFAULT_GAP = new Insets(2, 2, 2, 2);
    private static final Insets TOP_LEFT_GAP = new Insets(5, 5, 2, 2);
    private static final Insets TOP_GAP = new Insets(5, 2, 2, 2);

    private static final String VALUE_TITLE = "Value:";
    private static final String VARIABLE_TITLE = "Variable:";
    private static final String EXPRESSION_TITLE = "Expression:";
    private static final String ERRORS_TITLE = "Errors:";

    private static final String FAILED_TO_FILL_EXPRESSION = " failed to fill expression: ";
    private static final String FAILED_TO_FILL_COMBO_SELECTION = " failed to fill combo selection: ";

    private static final String GET_EXPRESSION = "GetExpression";
    private static final String ERRORS = "errors";
    private static final String LAST_CACHE_ERROR = "lastCacheError";
    private static final String LOG = "log";
    private static final String VERSION = "version";

    private JLabel valueLabel;
    private Label valueViewer;
    private JLabel variableLabel;
    private ComboBox variableSelection;
    private JLabel expressionLabel;
    private JTextArea expressionArea;
    private JLabel errorsLabel;
    private TextArea errorsArea;

    /** attribute value */
    private String attributeName;

    /**
     * Component that displays an attribute and two properties.
     */
    public TangoParserBean() {
        super();
        initialize();
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(final String valueAttribute) {
        attributeName = valueAttribute;
        if (attributeName != null) {
            cleanWidget(valueViewer);
            setWidgetModel(valueViewer, stringBox, generateAttributeKey(attributeName));
            fillExpression();
            variableSelection.setSelectedItem(attributeName);
        }
    }

    /**
     * This method initializes this
     */
    private void initialize() {
        setLayout(new GridBagLayout());
        setSize(476, 286);

        // label value
        valueLabel = new JLabel();
        valueLabel.setText(VALUE_TITLE);

        // champ value
        valueViewer = new Label();
        valueViewer.setPreferredSize(VALUE_SIZE);
        valueViewer.setMinimumSize(VALUE_SIZE);

        // label expression
        variableLabel = new JLabel();
        variableLabel.setText(VARIABLE_TITLE);

        // champ variable selection
        variableSelection = new ComboBox();
        variableSelection.addActionListener(this);

        // label where
        expressionLabel = new JLabel();
        expressionLabel.setText(EXPRESSION_TITLE);
        expressionLabel.setVerticalAlignment(SwingConstants.TOP);

        // label errors
        errorsLabel = new JLabel();
        errorsLabel.setText(ERRORS_TITLE);
        errorsLabel.setVerticalAlignment(SwingConstants.TOP);
        errorsArea = new TextArea();

        // champ where
        expressionArea = new JTextArea();
        expressionArea.setAutoscrolls(false);

        // value
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.insets = LEFT_GAP;
        add(valueLabel, constraints);
        constraints = new GridBagConstraints();
        constraints.gridx = 3;
        constraints.gridy = 2;
        constraints.insets = DEFAULT_GAP;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.weightx = 1.0D;
        constraints.weighty = 0.0D;
        constraints.ipadx = 0;
        constraints.ipady = 0;
        add(valueViewer, constraints);

        // variable selection
        constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.insets = TOP_LEFT_GAP;
        add(variableLabel, constraints);
        constraints = new GridBagConstraints();
        constraints.gridx = 3;
        constraints.gridy = 1;
        constraints.weightx = 1.0D;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.insets = TOP_GAP;
        add(variableSelection, constraints);

        // expression
        constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 3;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.insets = LEFT_GAP;
        add(expressionLabel, constraints);
        constraints = new GridBagConstraints();
        constraints.gridx = 3;
        constraints.gridy = 3;
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.insets = DEFAULT_GAP;
        add(expressionArea, constraints);

        // errors
        constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 4;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.insets = LEFT_GAP;
        add(errorsLabel, constraints);
        constraints = new GridBagConstraints();
        constraints.gridx = 3;
        constraints.gridy = 4;
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.insets = DEFAULT_GAP;
        add(errorsArea, constraints);

    }

    @Override
    protected void clearGUI() {
        variableSelection.removeAllItems();
        valueViewer.setVisible(false);
        cleanWidget(valueViewer);
        expressionArea.setText(ObjectUtils.EMPTY_STRING);
        cleanWidget(errorsArea);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        Object source = evt.getSource();
        if (source != null && source.equals(variableSelection) && evt.getModifiers() == ActionEvent.MOUSE_EVENT_MASK) {
            String value = (String) variableSelection.getSelectedItem();
            setAttributeName(value);
        }
    }

    @Override
    protected void refreshGUI() {
        if (model != null && !model.isEmpty()) {
            fillComboSelection();
            setAttributeName(attributeName);
            IKey key = generateAttributeKey(ERRORS);
            IDataSourceProducer producer = DataSourceProducerProvider.getProducer(key.getSourceProduction());
            boolean sourceOk = producer.isSourceCreatable(key);
            if (!sourceOk) {
                TangoKeyTool.registerAttribute(key, getModel(), LAST_CACHE_ERROR);
                sourceOk = producer.isSourceCreatable(key);
            }
            // Don't display errors zone if there is no error attribute
            if (sourceOk) {
                setErrorsVisible(true);
                setWidgetModel(errorsArea, stringBox, key);
            } else {
                setErrorsVisible(false);
            }
        }
    }

    private void setErrorsVisible(boolean visible) {
        errorsLabel.setVisible(visible);
        errorsArea.setVisible(visible);
        revalidate();
    }

    private void fillExpression() {
        if (attributeName != null) {
            try {
                DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(model);
                if (proxy != null) {
                    String[] values = TangoCommandHelper.executeCommand(proxy, GET_EXPRESSION, String[].class,
                            attributeName);
                    if (values != null) {
                        String expression = ObjectUtils.EMPTY_STRING;
                        for (String element : values) {
                            expression = expression + element + ObjectUtils.NEW_LINE;
                        }
                        expressionArea.setText(expression);
                    }
                }
            } catch (DevFailed e) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(TangoParserBean.this.getClass().getName()
                        + FAILED_TO_FILL_EXPRESSION + TangoExceptionHelper.getErrorMessage(e), e);
            }
        }
    }

    private void fillComboSelection() {
        variableSelection.removeAllItems();
        Collection<String> optionList = new ArrayList<>();
        try {
            DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(model);
            if (proxy != null) {
                String[] attributeList = proxy.get_attribute_list();
                if (attributeList != null) {
                    for (String element : attributeList) {
                        if (element != null && !element.equalsIgnoreCase(TangoAttributeHelper.STATE)
                                && !element.equalsIgnoreCase(TangoAttributeHelper.STATUS)
                                && !element.equalsIgnoreCase(ERRORS) && !element.equalsIgnoreCase(LAST_CACHE_ERROR)
                                && !element.equalsIgnoreCase(LOG) && !element.equalsIgnoreCase(VERSION)) {
                            if (attributeName == null) {
                                attributeName = element;
                            }
                            optionList.add(element);
                        }
                    }
                }
                Object[] options = optionList.toArray();
                optionList.clear();
                variableSelection.setValueList(options);
                variableSelection.setSelectedItem(attributeName);
            }
        } catch (DevFailed e) {
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(TangoParserBean.this.getClass().getName()
                    + FAILED_TO_FILL_COMBO_SELECTION + TangoExceptionHelper.getErrorMessage(e), e);
        }
    }

    @Override
    protected void onConnectionError() {
        clearGUI();
    }

    @Override
    protected void loadPreferences(Preferences arg0) {
        // not managed
    }

    @Override
    protected void savePreferences(Preferences arg0) {
        // not managed
    }

    /**
     * Test component with samba/parser/parser.1 device
     */
    public static void main(String... args) {
        JFrame frame = new JFrame();
        TangoParserBean bean = new TangoParserBean();
        if (args.length > 0) {
            boolean modelSet = false, attributeSet = false;
            for (String arg : args) {
                if (arg != null) {
                    String[] split = arg.split("/");
                    if (split.length == 3) {
                        bean.setModel(arg);
                        modelSet = true;
                    } else if (split.length == 4) {
                        int index = arg.lastIndexOf('/');
                        if (index < arg.length() - 1) {
                            bean.setModel(arg.substring(0, index));
                            modelSet = true;
                            bean.setAttributeName(arg.substring(index + 1));
                            attributeSet = true;
                        }
                    }
                }
            }
            if (!attributeSet) {
                bean.setAttributeName("condition1_ok");
            }
            if (modelSet) {
                bean.start();
            }
        }

        frame.setTitle("TangoParser");
        frame.setContentPane(bean);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

}
